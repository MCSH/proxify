const { Sequelize, Model, DataTypes } = require('sequelize');
const sequelize = new Sequelize(getDBConnectionString());

const NODE_ENV = process.env["NODE_ENV"] || "TEST";
const TEST = NODE_ENV === "TEST";

class JobM extends Model{}
JobM.init({
  url: DataTypes.STRING,
  status: {
    type: DataTypes.ENUM('NEW', 'PROCESSING', 'DONE', 'ERROR'),
    defaultValue: 'NEW',
  },
  http_code: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
  },
  worker_id: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
  }
}, { sequelize });

/**
   Get a databae connection
*/
async function initDB(){
  await sequelize.sync();
  // Test db initialization
  if(TEST){
    console.warn("\033[93mRunning in test mode, initializing database.\033[0m");
    await JobM.bulkCreate([
      {url: "https://proxify.io"},
      {url: "incorrect url"},
      {url: "https://reddit.com"},
    ]);
  }

  return {
    sequelize,
    JobM,
  };
}

module.exports = initDB;

// Utility functions
function getDBConnectionString(){
  if(process.env["DB_STRING"]) return process.env["DB_STRING"];
  console.warn("\033[91mENV variable DB_STRING was not provided, using in memory sqlite instead.\033[0m");
  return 'sqlite::memory:';
}
