const fetch = require('node-fetch');
const initDB = require('./model');

// Enviornment Variable
const WORKERID = process.env["WORKERID"] || randomIdGenerator();
const NODE_ENV = process.env["NODE_ENV"] || "TEST";
const TEST = NODE_ENV === "TEST";

// Main Logic
/**
   Perform a task

   Throw if there is an irrecoverable error that requires attention such as db disconnect.
*/
async function work(JobM){
  // This line might throw
  const [ rows ] = await JobM.update({
    status: 'PROCESSING',
    worker_id: WORKERID,
  }, {
    where: { status: 'NEW'},
    limit: 1,
  });
  if(rows == 0){
    // No more job available
    console.log("No more job available");
    return false;
  }
  const job = await JobM.findOne({where:{
    status: 'PROCESSING',
    worker_id: WORKERID,
  }});
  job.status = 'DONE';
  const status = await fetch(job.url)
        .then(res => res.status)
        .catch(()=>{
          job.status='ERROR';
          return 0;
        });
  job.http_code = status;

  await job.save();

  return true;
}

/**
   Main loop for worker function
*/
async function worker(){
  console.log("Initializing Worker.");

  const { JobM } = await initDB();


  // Main loop condition
  let isRunning = true;

  // Setup graceful shutdown
  process.on('SIGINT', () => {
    // We tell the worker not to do the next task if there was SIGINT (Ctrl+C) signal
    console.log("SIGINT recieved.");
    isRunning = false;
  });

  console.log("Starting the work loop.");
  while(isRunning){
    try{
      // if there aren't any job left, stop
      isRunning = await work(JobM);
    } catch (err){
      console.error(err);
      // on db connection error, stop
      isRunning = false;
    }
  }

  console.log("Shutting down.");
  if(TEST){
    console.log(await JobM.findAll());
  }
}

if(!process.env["NODE_ENV"])
  console.warn("\033[91mENV variable NODE_ENV was not provided, assuming TEST.\033[0m")

worker();


// Utility functions
function randomIdGenerator(){
  console.warn("\033[91mENV variable WORKERID was not provided, this could cause problems.\033[0m");
  const id = Math.floor(Math.random() * 5000);
  console.warn(`Generated id=${id}`);
  return id;
}
