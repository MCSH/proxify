# Distributed Workers

## Enivornment Variables

You can set the following ENVs to change system's behvaior:

### `WROKERID`

Set the worker id used in database to prevent repeating tasks. If not provided a random algorithm will generate a default that is printed.

### `NODE_ENV`

If set to `TEST`, the system will bulk insert 3 urls to the database. Default is "TEST"

### `DB_STRING`

The DB connection string, defaults to `"sqlite::memory:"`

## NOTE

To prevent unfinished tasks, the script captures SIGINT and shuts down only after the current task is finished.

A worker_id field was added to the database to keep track of workers.
